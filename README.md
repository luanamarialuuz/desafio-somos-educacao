# desafio-plurall-net

As necessidades abaixo precisam ser validadas e automatizadas.
1. O professor precisa criar uma turma com alunos.
2. O professor precisa recomendar 15 conteúdos para uma turma existente.
3. O professor precisa remover um usuário de uma turma específica.
4. O aluno precisa entrar em um curso de determinada disciplina e visualizar
uma aula

Plataforma para acesso: https://pt.khanacademy.org/
