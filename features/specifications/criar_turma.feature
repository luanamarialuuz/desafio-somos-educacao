#language: pt

Funcionalidade:  Criar uma turma com alunos 
Como professor do Khan Academy 
eu quero criar uma turma 
para ser administrador e poder adicionar os alunos 

Cenário: Criar uma turma 
Dado que insira o nome da turma
E que o aluno esteja cadastrado na plataforma
Quando a turma for criada
Entao o administrador poderá adicionar os alunos através do e-mail 