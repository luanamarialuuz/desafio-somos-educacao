#language: pt

Funcionalidade: Entrar no curso e visualizar uma aula 
Como acessando o Khan Academy com Login e senha
Eu quero entrar no curso
Para conseguir visualizar uma aula

Cenário: Assistir aula de um curso
Como aba de cursos escolher a escolaridade ou tema 
E assim os conteúdos serão listados 
Quando escolher o curso iniciar 
Para exibir por ordem o conteúdo do curso  
