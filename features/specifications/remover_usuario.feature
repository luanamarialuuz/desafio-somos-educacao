#language: pt

Funcionalidade: Remover usuário
Como professor administrador da turma
Eu quero remover um aluno
Para que ele não tenha, mas acesso a turma

Cenário: Remover aluno da turma
Como quando o professor administrador da turma entrar no menu
E listar os alunos 
Quando encontrar o nome do aluno terá uma opção de excluir
Para retirar o usuário da turma
